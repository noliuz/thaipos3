package com.bhahusut.thaipos;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class statMainFragment extends Fragment {
	Fragment frg;
	TextView todayTV,thisMonthTV;
	Button saveBT;
	ImageButton dayIB,monthIB;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.stat_main, container, false); 
		
		frg = this;
		todayTV = (TextView)rootView.findViewById(R.id.textView1);
		thisMonthTV = (TextView)rootView.findViewById(R.id.textView5);
		saveBT = (Button)rootView.findViewById(R.id.button1);
		dayIB = (ImageButton)rootView.findViewById(R.id.imageButton1);
		monthIB = (ImageButton)rootView.findViewById(R.id.imageButton2);
		
		dayIB.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                								
				/*
				LinearLayout ll = new LinearLayout(getActivity());
				ll.setOrientation(LinearLayout.HORIZONTAL);
				ll.setLayoutParams(new LinearLayout.LayoutParams(300,
						ViewGroup.LayoutParams.FILL_PARENT));
		        ll.setId(MainActivity.ll1_id);*/
				
				getActivity().getSupportFragmentManager().beginTransaction()
						.replace(MainActivity.ll1_id, 
								new sellListFragment()).commit();
				//MainActivity.mainLL.removeAllViews();
				//MainActivity.mainLL.addView(ll);
            }			
		});	
		
		saveBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                								
				saveDataCSV();
				MainActivity.showToast(getActivity(),"บันทึกเรียบร้อย");
            }			
		});		
		
		todayTV.setText("ยอดขายวันนี้ "+
				MainActivity.formatNum(totalSoldToday())+" บาท");
		thisMonthTV.setText("ยอดขายเดือนนี้ "+
				MainActivity.formatNum(totalSoldThisMonth())+" บาท");
		
		
		
		return rootView;
	}
	
	public int totalSoldToday() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String q = "SELECT sum(price*count) FROM sell WHERE dt>='"+
				sdf.format(new Date())+" 00:00:00' AND dt<='"+
				sdf.format(new Date())+" 23:59:59'";
		Cursor c = MainActivity.db1.query(q);
		c.moveToFirst();
		return c.getInt(0);		
	}
	
	public int totalSoldThisMonth() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-");
		String from_dt = sdf.format(new Date());
		String to_dt = from_dt;
		from_dt += "01";
		Calendar ca = Calendar.getInstance();
		to_dt += ca.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		
		String q = "SELECT sum(price*count) FROM sell WHERE ";
		q += "dt >= '"+from_dt+"'";
		q += " AND dt <= '"+to_dt+"'";
				
		Log.e("mm",q);
		
		Cursor c = MainActivity.db1.query(q);
		c.moveToFirst();
		return c.getInt(0);		
	}
	
	public void saveDataCSV() {
		//create file
		OutputStream fo;
			//file name
		SimpleDateFormat sdf = 
				new SimpleDateFormat("dd-MM-yyyy HHmmss");
		String fname = sdf.format(new Date());
		fname = "thaipos "+fname+".csv";
		File file = new File(MainActivity.db1.DATABASE_PATH
				+  fname);
		try {
			file.createNewFile();			
		} catch (Exception e) {
			Log.e("file",e.getMessage());
		}				
		
		//query
		String q = "SELECT id,product_name,count,price,dt"
				+ " FROM sell ORDER BY dt";
		Cursor c = MainActivity.db1.query(q);
		try {
			fo = new FileOutputStream(file);
			if (c.moveToFirst()) {
				do {
					String str,id,pname,count,price,dt;
					id = c.getString(0);
					pname = "\""+c.getString(1)+"\"";
					count = c.getString(2);
					price = c.getString(3);
					dt = "\""+c.getString(4)+"\"";
					
					str = id+","+pname+","+count+","+price+","+dt;
					str += "\n";
										
					fo.write(str.getBytes());					
				} while (c.moveToNext());			
			}
			fo.close();
		} catch (Exception e) {
			Log.e("file writing",e.getMessage());
		}		
		
	}
}
