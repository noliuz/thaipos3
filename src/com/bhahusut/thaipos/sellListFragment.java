package com.bhahusut.thaipos;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.bhahusut.thaipos.DataClass.Sell;

public class sellListFragment extends Fragment {	
	MainActivity act;
	TableLayout sellTL;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.sell_list, container, false);
		
		sellTL = (TableLayout)rootView.findViewById(R.id.tableLayout1);
		
		updateList();
		
		return rootView;
	}
	
	public void onAttach(MainActivity activity) {
	    super.onAttach(activity);
		act = activity;
					
	}
	
	public void updateList() {	
    	ArrayList<Sell> sellAL = new ArrayList<Sell>();    
    	
    	String q = "SELECT id,product_name,count,price,dt FROM sell";
    	q += " ORDER BY dt DESC";
    	    	    	
    	Cursor c = act.db1.query(q);   	
    	
    	if (c.moveToFirst()) {			
			do {
				Sell s = new Sell();
				s.id = c.getString(0);
				s.product_name = c.getString(1);
				s.count = c.getString(2);
				s.price = c.getString(3);
				s.dt = c.getString(4);
				sellAL.add(s);
			} while (c.moveToNext());			
		    	
    	}    	    	
    	//Log.e("xx",sellAL.size()+"");
    	
    	//add to table
    	sellTL.removeAllViews();
    	String prevDT="";    	
    	for (int i=0;i<sellAL.size();i++) {
    		if (!prevDT.equals(sellAL.get(i).dt)) {
    			//create row
    			TableRow tr = new TableRow(getActivity());
    				//params
    			android.widget.TableRow.LayoutParams p = new android.widget.TableRow.LayoutParams();
    			p.rightMargin = dpToPixel(10, getActivity()); // right-margin = 10dp
    			tr.setLayoutParams(p);

    			/*
    			//product name
    			TextView pnameTV = new TextView(getActivity());    			    			    			
    			pnameTV.setText(act.db1.getProductNameFromId(
    					sellAL.get(i).product_id));
    			tr.addView(pnameTV);
    			//count
    			TextView countTV = new TextView(getActivity());
    			countTV.setText(sellAL.get(i).count);
    			tr.addView(countTV);
    			//price
    			TextView priceTV = new TextView(getActivity());
    			priceTV.setText(sellAL.get(i).price);
    			tr.addView(priceTV);    			    			    			
    			*/
    			//dt
    			TextView dtTV = new TextView(getActivity());
    			dtTV.setText(MainActivity.dtEN2TH(sellAL.get(i).dt));
    			tr.addView(dtTV);
    			//view button
    			Button viewBT = new Button(getActivity());
    			
    				//button click
    			viewBT.setOnClickListener(new OnClickListener() {
    	            public void onClick(View v) {
    	            	TableRow ptr = (TableRow)v.getParent();
    	            	TextView dtTV = (TextView)ptr.getChildAt(0);
    	            	String dt = 
    	            		MainActivity.dtTH2EN((dtTV.getText().toString()));
    	            	
    	            	/*
    	            	LinearLayout ll2 = new LinearLayout(getActivity());
    	        		ll2.setOrientation(LinearLayout.HORIZONTAL);
    	        		ll2.setLayoutParams(new LinearLayout.LayoutParams(500,
    	        				ViewGroup.LayoutParams.FILL_PARENT));
    	                ll2.setId(MainActivity.ll2_id);*/
    	            	
    	            	Bundle bundle = new Bundle();    	            	
    	            	bundle.putString("readonly", "yes");    	            	
    	            	bundle.putString("dt",dt);
    	            	
    	            	//find sell_id    	            	    	            	
    	            	sellBillFragment sbf = new sellBillFragment();    	            	
    	            	sbf.setArguments(bundle);    	            	     	                	                    	                
    	            	    	                
    	            	FragmentTransaction transaction = getFragmentManager().beginTransaction();    		    
    	        	    transaction.replace(MainActivity.ll2_id, sbf);
    	        	    //transaction.addToBackStack(null);
    	        	    transaction.commit();
    	        	    
    	        	    //MainActivity.mainLL.addView(ll2);	
    	            }
    	        });
    			
    			viewBT.setText("ดู");
    			tr.addView(viewBT);
    			
    			sellTL.addView(tr);
    		}
    		prevDT = sellAL.get(i).dt;
    	}
    	    	    	
    }
	
	private static Float scale;
	public static int dpToPixel(int dp, Context context) {
	    if (scale == null)
	        scale = context.getResources().getDisplayMetrics().density;
	    return (int) ((float) dp * scale);
	}
	
	public String getSellIdFromDT(String dt) {
		String q = "SELECT id FROM sell WHERE dt LIKE '"+dt+"'";
		Cursor c =  MainActivity.db1.query(q);
		if (c.moveToFirst())
			return c.getString(0);
		return "1";
	}
	
}
