package com.bhahusut.thaipos;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

public class productDetailAddFragment extends Fragment {
	Activity act;
    Fragment frg;
    ArrayList<DataClass.Lot> lotAL;
    Spinner spinner1;
    EditText nameET,sell_priceET,lot_countET;
    TextView dateTV,timeTV;
    ImageButton dateBT,timeBT,submitBT,selProductBT,cancelBT;
    private static final int req_pname = 100;        
    productSelFragment psf;
    datepickerDialogFragment dpDF;
    timepickerDialogFragment tpTP;
    
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.product_detail_add, container, false); 
		
		frg = this;
		
		submitBT = (ImageButton)rootView.findViewById(R.id.imageButton2);
		nameET = (EditText)rootView.findViewById(R.id.editText1);
		sell_priceET = (EditText)rootView.findViewById(R.id.editText2);
		lot_countET = (EditText)rootView.findViewById(R.id.editText3);
		//dtTV = (TextView)rootView.findViewById(R.id.textView5);
		dateTV = (TextView)rootView.findViewById(R.id.textView7);
		timeTV = (TextView)rootView.findViewById(R.id.textView8);
		dateBT = (ImageButton)rootView.findViewById(R.id.imageButton1);
		timeBT = (ImageButton)rootView.findViewById(R.id.imageButton5);		
		selProductBT = (ImageButton)rootView.findViewById(R.id.imageButton3);
		cancelBT = (ImageButton)rootView.findViewById(R.id.imageButton4);
		
		//get current dt
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		dateTV.setText(
				MainActivity.dateEN2TH(sdf.format(new Date())));		
		sdf = new SimpleDateFormat("HH:mm:ss");
		timeTV.setText(sdf.format(new Date()));
		
		selProductBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                								
				FragmentManager fm = frg.getFragmentManager();
                psf = new productSelFragment();
                
                psf.setTargetFragment(frg,req_pname);
                
                psf.show(fm, "เลือกสินค้า");
            }			
		});
		
		submitBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                								
				//validation
				if (formValidation()) {				
					addProduct();
					sendResult(Activity.RESULT_OK,"yes");
					getActivity().getSupportFragmentManager()
						.beginTransaction().remove(frg)
						.commit();
				}
            }			
		});
		
		cancelBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                											
				sendResult(Activity.RESULT_OK,"yes");
				getActivity().getSupportFragmentManager()
					.beginTransaction().remove(frg)
					.commit();
            }			
		});		
		
		dateBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                				
				dpDF = new datepickerDialogFragment();
				
				dpDF.setTargetFragment(frg,1);
				
				dpDF.show(getFragmentManager().beginTransaction(),"เลือกวัน");
            }			
		});
		
		timeBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {
				tpTP = new timepickerDialogFragment();
				
				tpTP.setTargetFragment(frg,2);
				
				tpTP.show(getFragmentManager().beginTransaction(),"เลือกเวลา");											
            }			
		});				
			
		psf = new productSelFragment();
		dpDF = new datepickerDialogFragment();
		tpTP = new timepickerDialogFragment();
		
    	return rootView;
    }
    
	public void onAttach(Activity activity) {
	    super.onAttach(activity);
		act = activity;								
	}
	    
	public void updateDetail() {
				
	}
	

	public void addProduct() {					
	
		String l_name = nameET.getText().toString();
		String l_sell_price = sell_priceET.getText().toString();
		String l_lot_count = lot_countET.getText().toString();
		String l_dt = getUIDateTime();				
		
		//add product
		String q = "INSERT INTO product (name,sell_price,lot_count,dt) VALUES ('";
		q += l_name+"'"+",'";
		q += l_sell_price+"'"+",'";
		q += l_lot_count+"'"+",'";
		q += l_dt+"')";
					
		MainActivity.db1.query_no_res(q);
	}	

	void sendResult(int resultCode,String isOk) {
    	if (getTargetFragment() == null)
    		return;
    	Intent i = new Intent();
    	i.putExtra("isOk", isOk);
    	
    	getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode,
    			i);
    }
		
	public boolean formValidation() {
		String l_name = nameET.getText().toString();
		String l_sell_price = sell_priceET.getText().toString();
		String l_lot_count = lot_countET.getText().toString();
		String l_dt = getUIDateTime();
		
		if (l_name.isEmpty() || l_sell_price.isEmpty()
				|| l_lot_count.isEmpty() || l_dt.isEmpty()) {
			MainActivity.showToast(getActivity(),"กรุณากรอกข้อมูลให้ครบ");
			return false;
		}
		
		return true;
	}
	
	public void updateDate(String date) {								
		String[] dateArr = date.split("-");
		int y = Integer.parseInt(dateArr[0]);
		int m = Integer.parseInt(dateArr[1]);
		int da = Integer.parseInt(dateArr[2]);
		
		Log.e("xx",y+"");
		
		Calendar c;
		c = Calendar.getInstance();
		c.set(y, m,da);
		DateFormat df = new DateFormat();		
		String enDate = df.format("yyyy-MM-dd",c.getTime()).toString();	
		
		dateTV.setText(MainActivity.dateEN2TH(enDate));		
	}
	
	public void updateTime(String time) {
		DateFormat df = new DateFormat();
		Date d = new Date();
		
		String[] timeArr = time.split(":");
		int h = Integer.parseInt(timeArr[0]);
		int m = Integer.parseInt(timeArr[1]);
		d.setHours(h);
		d.setMinutes(m);
		
		timeTV.setText(df.format("hh:mm:ss",d));		
	}
	
	
	
	public void setUIDateTime(String enDT) {
		String[] dtArr = enDT.split(" ");
		String date = dtArr[0];
		String time = dtArr[1];
		
		updateTime(time);
		updateDate(date);
	}
	
	public String getUIDateTime() {
		String date = MainActivity.dateTH2EN(dateTV.getText().toString());
		String time = timeTV.getText().toString();
		
		return date+" "+time;
	}
	
	@Override
	public void onActivityResult(int requestCode,int resultCode,Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
						
		if (resultCode != Activity.RESULT_OK) return;				
		if (requestCode == req_pname) {
			String pname = (String)data.getSerializableExtra("pname");
			nameET.setText(pname);
		} else if (requestCode == 1) {//datepicker
			String date = (String)data.getSerializableExtra("date");	
			updateDate(date);
		} else if (requestCode == 2) {//timepicker
			String time = (String)data.getSerializableExtra("time");
			updateTime(time);
		} 						
	}	
	
	@Override
	public void onSaveInstanceState (Bundle outState) {
		super.onSaveInstanceState(outState);

		
		if (psf != null) {
			psf.setTargetFragment(null,-1);
		}
		if (dpDF != null) {
			dpDF.setTargetFragment(null,-1);
		}
		if (tpTP != null) {
			tpTP.setTargetFragment(null,-1);
		}
		
		
		
	}
	
	@Override
	public void onResume () {
		super.onResume();
				
		if (psf != null) {
			psf.setTargetFragment(frg,req_pname);
		}
		
		if (dpDF != null) {
			dpDF.setTargetFragment(frg,1);
		}
		
		if (tpTP != null) {
			tpTP.setTargetFragment(frg,2);
		}	
	}
	
	@Override
	public void onPause() {
		super.onPause();
		clearFragment();	
	}

	public void clearFragment() {
		FragmentManager fm = getFragmentManager();
		fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);		
	}
	
}
