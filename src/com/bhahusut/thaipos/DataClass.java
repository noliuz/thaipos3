package com.bhahusut.thaipos;

public class DataClass {	
	
	public static class Lot {
		public String id;
		public String name;
		public String sell_price;
		public String lot_count;	
		public String dt;	
				
	}
	
	public static class Sell {
		public String id;
		public String product_name;
		public String price;
		public String count;
		public String dt;
		
	}
}
