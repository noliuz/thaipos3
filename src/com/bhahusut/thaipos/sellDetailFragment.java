package com.bhahusut.thaipos;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class sellDetailFragment extends Fragment {
    Activity act;
    ImageButton selProductBT,updateBT,delBT;    
	TextView pnameTV,countTV;
	private static final int req_pname = 0;
    String sell_id="1",product_name;
	EditText countET,priceET;
	Fragment frg;
	boolean fromReadonly = false;
	String dtBundle = "";
	productSelFragment psf;
    	
	@Override
	public void onActivityResult(int requestCode,int resultCode,Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
						
		if (resultCode != Activity.RESULT_OK) return;				
		String pname = (String)data.getSerializableExtra("pname");
		pnameTV.setText(pname);		
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.sell_detail, container, false); 
				
		frg = this;
		selProductBT = (ImageButton)rootView.findViewById(R.id.imageButton1);
		updateBT = (ImageButton)rootView.findViewById(R.id.imageButton2);
		delBT = (ImageButton)rootView.findViewById(R.id.imageButton3);
		pnameTV = (TextView)rootView.findViewById(R.id.textView2);
		countTV = (TextView)rootView.findViewById(R.id.textView3);
		countET = (EditText)rootView.findViewById(R.id.editText1);
		priceET = (EditText)rootView.findViewById(R.id.editText2);
					
		
		countET.setOnFocusChangeListener(new OnFocusChangeListener() {          

	        public void onFocusChange(View v, boolean hasFocus) {	        	
            	String pname  = pnameTV.getText().toString();        	    
            	int soldCount = MainActivity.db1.getSoldCount(product_name);
        	    int haveCount = MainActivity.db1.getTotalHave(product_name);
        	    int leftCount = haveCount - soldCount;
	        	if(!hasFocus) {
	        		String countStr = countET.getText().toString();
	        		if (!countStr.isEmpty()) {	        			
		        		int count = Integer.parseInt(countStr);	        		
		        		if (leftCount < count) {
		        			countET.setText("0");
		        			countET.requestFocus();
		        		}
	        		}
	            }	                
	        }
	    });
		
		delBT.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	String q = "DELETE FROM sell WHERE id="+sell_id;
            	            	           	
            	MainActivity.db1.query_no_res(q);
            	            	
            	if (fromReadonly) {
            		showSellBillFragment();
            	} else { 
            		sendResult(Activity.RESULT_OK,"yes");
            		getActivity().getSupportFragmentManager()
						.beginTransaction().remove(frg).commit();
            	}
            }
        });
		
		updateBT.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	if (!formValidation())
            		return;
            	String pcount = countET.getText().toString();
            	String pprice = priceET.getText().toString();
            	
            	String q = "UPDATE sell SET ";
            	q += "product_name='"+product_name+"',";
            	q += "price="+pprice+",";
            	q += "count="+pcount;
            	q += " WHERE id="+sell_id;
            	           	
            	MainActivity.db1.query_no_res(q);
            	            	            	
            	if (fromReadonly) {
            		showSellBillFragment();            		
            	} else { 
            		sendResult(Activity.RESULT_OK,"yes");
            		getActivity().getSupportFragmentManager()
						.beginTransaction().remove(frg).commit();
            	}
            }
        });
		
		selProductBT.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	 FragmentManager fm = frg.getFragmentManager();
                 psf = new productSelFragment();
                 psf.setTargetFragment(sellDetailFragment.this,req_pname);
                 psf.show(fm, "เลือกสินค้า");                
            }
        });
		
		sell_id = getArguments().getString("sell_id");
		if (getArguments().containsKey("readonly")) {
			fromReadonly = true;
			dtBundle = getArguments().getString("dt");
		}
		updateDetail();
		
    	return rootView;
    }
    
	public void onAttach(Activity activity) {
	    super.onAttach(activity);
		act = activity;						
	}
	
	public void updateDetail() {
		//get sell detail from id
		String q  = "SELECT product_name,count,price FROM sell WHERE id="+sell_id;
		Cursor c = MainActivity.db1.query(q);
		c.moveToFirst();
		product_name = c.getString(0);
		String pname = product_name;
		String count = c.getString(1);
		String price = c.getString(2);
		
		pnameTV.setText(pname);
		countET.setText(count);
		priceET.setText(price);
		updateCountLeft(product_name);
	
	}
	
	void sendResult(int resultCode,String isOk) {
    	if (getTargetFragment() == null)
    		return;
    	Intent i = new Intent();
    	i.putExtra("isOk", isOk);
    	
    	getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode,
    			i);
    }
	
	void updateCountLeft(String pname) {
		//get left_count from db
	    int soldCount = MainActivity.db1.getSoldCount(pname);
	    int haveCount = MainActivity.db1.getTotalHave(pname);
	    int leftCount = haveCount - soldCount;
	    countTV.setText("จำนวน("+leftCount+")");
	}
	
	public boolean formValidation() {
		String pcount = countET.getText().toString();
    	String pprice = priceET.getText().toString();
		
		if (product_name.isEmpty() || pcount.isEmpty() 
				||pprice.isEmpty() || product_name.equals("เลือกสินค้า")) {
			MainActivity.showToast(getActivity(),"กรุณากรอกข้อมูลให้ครบ");
			return false;
		}
		
		return true;
	}
	
	public void showSellBillFragment() {
		/*
		LinearLayout ll2 = new LinearLayout(getActivity());
		ll2.setOrientation(LinearLayout.HORIZONTAL);
		ll2.setLayoutParams(new LinearLayout.LayoutParams(500,
				ViewGroup.LayoutParams.FILL_PARENT));
        ll2.setId(MainActivity.ll2_id);*/
    	
    	Bundle bundle = new Bundle();    	            	
    	bundle.putString("readonly", "yes");    	            	
    	bundle.putString("dt",dtBundle);
    		    	            	
    	sellBillFragment sbf = new sellBillFragment();    	            	
    	sbf.setArguments(bundle);    	            	     	                	                    	                
    	    	                
    	FragmentTransaction transaction = getFragmentManager().beginTransaction();    		    
	    transaction.replace(MainActivity.ll2_id, sbf);
	    //transaction.addToBackStack(null);
	    transaction.commit();
	    
	    //MainActivity.mainLL.addView(ll2);
	}
	
	@Override
	public void onSaveInstanceState (Bundle outState) {		
		super.onSaveInstanceState(outState);
		
		if (psf != null) {
			psf.setTargetFragment(null,-1);
		}
	}
		
	@Override
	public void onResume () {					
		super.onResume();
				
		if (psf != null) {
			psf.setTargetFragment(sellDetailFragment.this,req_pname);
		}		
	}

}
