package com.bhahusut.thaipos;

import java.util.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TimePicker;

public class timepickerDialogFragment extends DialogFragment {
	ImageButton okIB,cancelIB;
	TimePicker tpTP;
	DialogFragment frg;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.timepicker_dialog, container, false);
        
        frg = this;
        okIB = (ImageButton)v.findViewById(R.id.imageButton1);
        cancelIB = (ImageButton)v.findViewById(R.id.imageButton2);
        tpTP = (TimePicker)v.findViewById(R.id.timePicker1);
        
        cancelIB.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                								
				frg.dismiss();
            }			
		});
        
        okIB.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                								
				sendResult(Activity.RESULT_OK);
				frg.dismiss();
            }			
		});
        
        //set current time        
        Calendar cal=Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);
        tpTP.setCurrentHour(hour);
        tpTP.setCurrentMinute(min);               
        
        return v;
    }
	
	public void sendResult(int resultCode) {
    	if (getTargetFragment() == null)
    		return;
    	
    	int hour  = tpTP.getCurrentHour();
    	int min = tpTP.getCurrentMinute();
    	
    	
    	String time = hour+":"+min;
    	
    	Intent i = new Intent();
    	i.putExtra("time",time);
    	
    	getTargetFragment().onActivityResult(2, resultCode,
    			i);
    }
}
