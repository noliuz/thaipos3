package com.bhahusut.thaipos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class sellDetailAddFragment extends Fragment {
	Activity act;
    ImageButton selProductBT,addBT,cancelBT;    
	TextView pnameTV,countTV;
	private static final int req_pname = 0;
    String sell_id="1",product_name;
	EditText countET,priceET;
	Fragment frg;	
	productSelFragment psf;	
	
	@Override
	public void onActivityResult(int requestCode,int resultCode,Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
						
		if (resultCode != Activity.RESULT_OK) return;				
		String pname = (String)data.getSerializableExtra("pname");
		pnameTV.setText(pname);	
		updateCountLeft(pname);
		priceET.setText(MainActivity.db1.getPriceFromName(pname)+"");
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.sell_detail_add, container, false); 
				
		frg = this;
		selProductBT = (ImageButton)rootView.findViewById(R.id.imageButton1);
		addBT = (ImageButton)rootView.findViewById(R.id.imageButton2);		
		cancelBT = (ImageButton)rootView.findViewById(R.id.imageButton3);
		pnameTV = (TextView)rootView.findViewById(R.id.textView2);
		countTV = (TextView)rootView.findViewById(R.id.textView3);
		countET = (EditText)rootView.findViewById(R.id.editText1);
		priceET = (EditText)rootView.findViewById(R.id.editText2);
				
		countET.setOnFocusChangeListener(new OnFocusChangeListener() {          
	        public void onFocusChange(View v, boolean hasFocus) {	        	
            	String pname  = pnameTV.getText().toString();        	    
            	int soldCount = MainActivity.db1.getSoldCount(pname);
        	    int haveCount = MainActivity.db1.getTotalHave(pname);
        	    int leftCount = haveCount - soldCount;
	        	if(!hasFocus) {
	        		String countStr = countET.getText().toString();
	        		if (!countStr.isEmpty()) {
		        		int count = Integer.parseInt(countStr);	        		
		        		if (leftCount < count) {
		        			countET.setText("0");
		        			countET.requestFocus();
		        		}
	        		}
	            }	                
	        }
	    });
		
		cancelBT.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	sendResult(Activity.RESULT_OK,"yes");
            	getActivity().getSupportFragmentManager()
				.beginTransaction().remove(frg).commit();
            }
		});
		
		addBT.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	if (!formValidation())
            		return;
            	String pcount = countET.getText().toString();
            	String pprice = priceET.getText().toString();
            	String pname = pnameTV.getText().toString();
            	
            	String q = "INSERT INTO sell (product_name,price,count,dt)"
            			+ " VALUES (";
            	q += "'"+pname+"',";
            	q += "'"+pprice+"',";
            	q += "'"+pcount+"','')";
            	       	
            	
            	MainActivity.db1.query_no_res(q);
            	
            	sendResult(Activity.RESULT_OK,"yes");
            	getActivity().getSupportFragmentManager()
				.beginTransaction().remove(frg).commit();
            }
        });
		
		selProductBT.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	 FragmentManager fm = frg.getFragmentManager();
                 psf = new productSelFragment();
                 psf.setTargetFragment(sellDetailAddFragment.this,req_pname);
                 psf.show(fm, "เลือกสินค้า");                
            }
        });								
		
    	return rootView;
    }
    
	public void onAttach(Activity activity) {
	    super.onAttach(activity);
		act = activity;						
	}		
	
	void sendResult(int resultCode,String isOk) {
    	if (getTargetFragment() == null)
    		return;
    	Intent i = new Intent();
    	i.putExtra("isOk", isOk);
    	
    	getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode,
    			i);
    }
	
	void updateCountLeft(String pname) {
		//get left_count from db
	    int soldCount = MainActivity.db1.getSoldCount(pname);
	    int haveCount = MainActivity.db1.getTotalHave(pname);
	    int leftCount = haveCount - soldCount;
	    countTV.setText("คงเหลือ("+leftCount+")");
	}
	
	public boolean formValidation() {
		String pcount = countET.getText().toString();
    	String pprice = priceET.getText().toString();
    	String pname = pnameTV.getText().toString();
		
		if (pname.isEmpty() || pcount.isEmpty() 
				||pprice.isEmpty() || pname.equals("เลือกสินค้า")) {
			MainActivity.showToast(getActivity(),"กรุณากรอกข้อมูลให้ครบ");
			return false;
		}
		
		return true;
	}
	
	@Override
	public void onSaveInstanceState (Bundle outState) {		
		super.onSaveInstanceState(outState);
		
		if (psf != null) {
			psf.setTargetFragment(null,-1);
		}
	}
		
	@Override
	public void onResume () {					
		super.onResume();
		
		if (psf != null) {
			psf.setTargetFragment(sellDetailAddFragment.this,req_pname);
		}		
	}

	
}
