package com.bhahusut.thaipos;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

public class db extends SQLiteOpenHelper {
	
	private static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "db.sqlite";
	public static String DATABASE_PATH = "/mnt/shared/shared/";    

	CursorFactory cfactory;
	SQLiteDatabase sqliteDB;
	Activity act;
	
	public db(Activity context) {
		super(context,DATABASE_NAME,null,DATABASE_VERSION);
		
		act = context;
		
		try {
			//for deploy
			Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
			if (isSDPresent) {
				DATABASE_PATH = 
						Environment.getExternalStorageDirectory().getPath()
						+"/thaipos/";
			}
			else {
				DATABASE_PATH = act.getFilesDir()+						
						"/thaipos/";
			}						
			
			Log.e("db",DATABASE_PATH);
			
			createFolder();
			createDBFile();
			
			String path = DATABASE_PATH+DATABASE_NAME; 
			sqliteDB = SQLiteDatabase.openDatabase(path, 
				null, SQLiteDatabase.OPEN_READWRITE);
			Log.e("db",path);
		} catch (SQLiteException e) {
			Log.e("db",e.getLocalizedMessage());
		}
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion) {
		
	}
	
	public Cursor query(String sql) {
		return sqliteDB.rawQuery(sql,null); 
	}
	
	public void query_no_res(String sql) {
		try {	
			sqliteDB.execSQL(sql);
		}  catch (Exception e) {
			MainActivity.showToast(act, e.getMessage());
		}
	}
	
	public void dumpTable(String tname) {
		String q = "SELECT * FROM "+tname;
		Cursor c = query(q);
		
		if (c.moveToFirst()) {
			int colCount = c.getColumnCount();
			do {
				ArrayList<String> sbuf = new ArrayList<String>();
				
				sbuf.add(c.getInt(0)+"");
				for (int i=1;i<colCount;i++) {
					sbuf.add(c.getString(i));
				}
								
				String tmp = "";
				for (int i=0;i<sbuf.size();i++)
					tmp += sbuf.get(i)+",";
				tmp = tmp.substring(0, tmp.length()-1);
				
				Log.e("dump_"+tname,tmp);				
			} while (c.moveToNext());
			
		}
	}
			
	public void addProduct(String name,int sell_price,int lot_count,String dt) {
		String q = "INSERT INTO product (name,sell_price,lot_count,dt) VALUES (";
		q += "'"+name+"'"+",";
		q += "'"+sell_price+"'"+",";
		q += "'"+lot_count+"'"+",";
		q += dt;
		q += ")";
		query_no_res(q);
	}
	
	public void addSell(int product_id,int price,int count,String dt) {
		String q = "INSERT INTO sell (product_id,price,count,dt) VALUES (";
		q += "'"+product_id+"'"+",";
		q += "'"+price+"'"+",";
		q += "'"+count+"'"+",";
		q += dt;		
		q += ")";
		query_no_res(q);
	}
	
	public void deleteRow(int id,String tname) {
		String q = "DELETE FROM "+tname+" WHERE id="+id;
		query_no_res(q);	
	}
	
	public String getProductNameFromId(String id) {
		String q = "SELECT name FROM product WHERE id="+id;
		Cursor c = query(q);
		if (c.getCount() == 0) {
			return "none";
		} else {
			c.moveToFirst();
			return c.getString(0);
		}		
	}
	
	public int getSoldCount(String pname) {
		//get sold count
    	String q = 
    		"SELECT sum(count) FROM sell WHERE product_name LIKE "
    				+"'"+pname+"'";
    	Cursor c = query(q);
    	c.moveToFirst();
    	return c.getInt(0);
	}
	
	public int getTotalHave(String pname) {
		//get total have
    	String q = 
	      "SELECT sum(lot_count) FROM product WHERE name LIKE "
	    				+"'"+pname+"'";
    	//Log.e("db",q);
    	Cursor c1 = query(q);
    	c1.moveToFirst();
    	return c1.getInt(0);    	    		
	}
	
	public int getPriceFromName(String pname) {
		String q = "SELECT max(sell_price) FROM product WHERE name LIKE "
				+"'"+pname+"'";
		Cursor c1 = query(q);
    	c1.moveToFirst();
    	return c1.getInt(0);
	}
	
	public void truncateDB() {
		sqliteDB.close();
		String path = DATABASE_PATH+DATABASE_NAME;
		
		File f = new File(path);
		f.delete();
		
		createDBFile();
				 
		sqliteDB = SQLiteDatabase.openDatabase(path, 
			null, SQLiteDatabase.OPEN_READWRITE);
				
	}	
	
	public void createFolder() {
		try {
			File f = new File(DATABASE_PATH);
			if (!f.isDirectory())
				f.mkdir();
		} catch (Exception e) {
			MainActivity.showToast(act, e.getMessage());
		}
	}
	
	public void createDBFile() {
		try {
			createFolder();
			//chk if db file existed
			File f = new File(DATABASE_PATH+DATABASE_NAME);
			if (!f.exists()) {
				SQLiteDatabase db =  SQLiteDatabase.openOrCreateDatabase(
						DATABASE_PATH+DATABASE_NAME,null);
				String q = " CREATE TABLE product "
						+ "(id INTEGER PRIMARY KEY, name TEXT, "
						+ "sell_price INTEGER, lot_count INTEGER, dt TEXT)";
				db.execSQL(q);
				q = "CREATE TABLE sell (saved TEXT, count NUMERIC,"
						+ " id INTEGER PRIMARY KEY, product_name TEXT,"
						+ " price INTEGER, dt TEXT)";
				db.execSQL(q);
				db.close();
			}
		}  catch (Exception e) {
			MainActivity.showToast(act, e.getMessage());
		}
	}
	
}