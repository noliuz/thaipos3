package com.bhahusut.thaipos;

import java.util.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;

public class datepickerDialogFragment extends DialogFragment {

	ImageButton okIB,cancelIB;
	DatePicker dpDP;
	DialogFragment frg;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.datepicker_dialog, container, false);
        
        frg = this;
        okIB = (ImageButton)v.findViewById(R.id.imageButton1);
        cancelIB = (ImageButton)v.findViewById(R.id.imageButton2);
        dpDP = (DatePicker)v.findViewById(R.id.datePicker1);
        
        cancelIB.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                								
				frg.dismiss();
            }			
		});
        
        okIB.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                								
				sendResult(Activity.RESULT_OK);
				frg.dismiss();
            }			
		});
        
        //set current date
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);        
        dpDP.init(year,month,day,null);
        
        return v;
    }
	
	public void sendResult(int resultCode) {
    	if (getTargetFragment() == null)
    		return;
    	
    	int day  = dpDP.getDayOfMonth();
    	int month = dpDP.getMonth();
    	int year = dpDP.getYear();
    	
    	String date = year+"-"+month+"-"+day;
    	
    	Intent i = new Intent();
    	i.putExtra("date", date);
    	
    	getTargetFragment().onActivityResult(1, resultCode,
    			i);
    }
}