package com.bhahusut.thaipos;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.bhahusut.thaipos.DataClass.Sell;

public class sellBillFragment extends Fragment {
	TableLayout billTL;
	String dtBundle = "";
	ArrayList<Sell> sellAL;
	Fragment frg;
	ImageButton addBT,saveBT;
	boolean isReadonly = false;
	sellDetailFragment sbf;
	sellDetailAddFragment sdaf;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.sell_bill, container, false);
		
		frg = this;
		billTL = (TableLayout)rootView.findViewById(R.id.tableLayout1);
		addBT = (ImageButton)rootView.findViewById(R.id.imageButton1);
		saveBT = (ImageButton)rootView.findViewById(R.id.imageButton2);
		
		addBT.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	addSellDetailAddFragment();
            }
        });
		
		saveBT.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {            	            	
            	//save dt
            	String q = "UPDATE sell SET dt=datetime('now') WHERE dt LIKE ''";
            	MainActivity.db1.query_no_res(q);
            	
            	updateList();
            }
        });

		if (getArguments() != null) {		
			if (getArguments().containsKey("readonly")) {
				isReadonly = true;
				disableButton();
			}
			if (getArguments().containsKey("dt"))
				dtBundle = getArguments().getString("dt");
		}
		
		updateList();			
		
		return rootView;
	}
	
	public void onAttach(Activity activity) {
	    super.onAttach(activity);
							
	}
	
	public void updateList() {	
		sellAL = new ArrayList<Sell>();    
    	
    	String q = "SELECT id,product_name,count,price FROM sell "
    			+ "WHERE dt LIKE ''";

    	if (!dtBundle.isEmpty())
    		q = "SELECT id,product_name,count,price FROM sell "
        			+ "WHERE dt LIKE '"+dtBundle+"'";
    	
    	Log.e("cc",q);
    	
    	Cursor c = MainActivity.db1.query(q);   	    	
    	if (c.moveToFirst()) {						
    		do {
				Sell s = new Sell();
				s.id = c.getString(0);
				s.product_name = c.getString(1);
				s.count = c.getString(2);
				s.price = c.getString(3);				
				sellAL.add(s);								
			} while (c.moveToNext());					    
    	}    	    	
    	
    	Log.e("xx",c.getCount()+"");
    	    	
    	billTL.removeAllViews();
    	
    	//add header row
    	TableRow tr1 = new TableRow(getActivity());
    	
    	
    	TableRow.LayoutParams lp1 = 
    			new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,
    					LayoutParams.WRAP_CONTENT);
    	
		DisplayMetrics displayMetrics = 
				getActivity().getResources().getDisplayMetrics();
		int pixel = (int)((5 * displayMetrics.density) + 0.5);
		lp1.rightMargin = pixel;				
		
		TextView hnameTV = new TextView(getActivity());		
		hnameTV.setLayoutParams(lp1);				
		hnameTV.setText(createTableHeaderString("ชื่อสินค้า"));
		tr1.addView(hnameTV);
						
		TextView hcountTV = new TextView(getActivity());
		hcountTV.setLayoutParams(lp1);
		hcountTV.setText(createTableHeaderString("จำนวน"));
		tr1.addView(hcountTV);
		
		TextView hpriceTV = new TextView(getActivity());
		hpriceTV.setLayoutParams(lp1);
		hpriceTV.setText(createTableHeaderString("ราคา"));
		tr1.addView(hpriceTV);				
		
		billTL.addView(tr1);
		
		int total_price=0;
		
    	for (int i=0;i<sellAL.size();i++) {
    	    	
    		String count = sellAL.get(i).count;
    		String price = sellAL.get(i).price;
    		total_price += Integer.parseInt(count)*Integer.parseInt(price);
    		
    		
			//create row
			TableRow tr = new TableRow(getActivity());					
			
			//product name
			TextView pnameTV = new TextView(getActivity());    			    			    			
			pnameTV.setText(sellAL.get(i).product_name);
			pnameTV.setLayoutParams(lp1);
			tr.addView(pnameTV);
			
			//count
			TextView countTV = new TextView(getActivity());
			countTV.setText(sellAL.get(i).count);
			countTV.setLayoutParams(lp1);
			tr.addView(countTV);
			
			//price
			TextView priceTV = new TextView(getActivity());
			priceTV.setText(MainActivity.formatNum(sellAL.get(i).price));		
			priceTV.setLayoutParams(lp1);
			
			tr.addView(priceTV);    			    	
			
			
			//view button
			ImageButton viewBT = new ImageButton(getActivity());						
			viewBT.setImageResource(android.R.drawable.ic_menu_edit);			
				//button size			 				
			
			final float scale = getActivity().getResources()
									.getDisplayMetrics().density;
			int pixels = (int) (35 * scale + 0.5f);
			TableRow.LayoutParams lp = new TableRow.LayoutParams(pixels, pixels);			
			viewBT.setLayoutParams(lp);
						
				//button click
			final int idx = i;
			viewBT.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	            	addDetailFragment(v,idx);
	            }
	        });						
			tr.addView(viewBT);
			
			billTL.addView(tr);						
    	}    
    	
    	//add total				
			//add to table layout
		TableRow tr = new TableRow(getActivity());
			//total text
		TextView ttextTV = new TextView(getActivity());
		ttextTV.setText(this.createTableHeaderString("รวม"));		
		ttextTV.setLayoutParams(lp1);			
		tr.addView(ttextTV);
			//price
		TextView totalTV = new TextView(getActivity());
		totalTV.setText(MainActivity.formatNum(total_price+""));		
		totalTV.setLayoutParams(lp1);			
		tr.addView(totalTV);
		
		billTL.addView(tr);
    }
	
	public void addDetailFragment(View v,int idx) {
		TableRow ptr = (TableRow)v.getParent();
    	TextView dtTV = (TextView)ptr.getChildAt(0);
    	String dt = dtTV.getText().toString();
    	
    	Bundle bundle = new Bundle();
    	bundle.putString("sell_id",sellAL.get(idx).id);
    	if (isReadonly) {
    		bundle.putString("readonly","yes");
    		bundle.putString("dt",dtBundle);    		
    	}
    	
    	sbf = new sellDetailFragment();
    	sbf.setArguments(bundle);
    	sbf.setTargetFragment(frg,0);
    	
    	/*
	    LinearLayout ll2 = new LinearLayout(getActivity());
		ll2.setOrientation(LinearLayout.HORIZONTAL);
		ll2.setLayoutParams(new LinearLayout.LayoutParams(500,
				ViewGroup.LayoutParams.FILL_PARENT));
        ll2.setId(MainActivity.ll2_id);*/
		
		getActivity().getSupportFragmentManager().beginTransaction()
				.replace(MainActivity.ll2_id, sbf).commit();					
													
		//MainActivity.mainLL.addView(ll2);			
	}
	
	@Override
	public void onActivityResult(int requestCode,int resultCode,Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
						
		if (resultCode != Activity.RESULT_OK) return;				
		String isOk = (String)data.getSerializableExtra("isOk");
		Log.e("xx",isOk);
		if (isOk.equals("yes"))
			updateList();				
	}
	
	public void addSellDetailAddFragment() {
    	/*
		LinearLayout ll2 = new LinearLayout(getActivity());
		ll2.setOrientation(LinearLayout.HORIZONTAL);
		ll2.setLayoutParams(new LinearLayout.LayoutParams(500,
				ViewGroup.LayoutParams.FILL_PARENT));
        ll2.setId(MainActivity.ll2_id);*/
													
	    sdaf = new sellDetailAddFragment();    		        		 
	    sdaf.setTargetFragment(frg, 0);
	    FragmentTransaction transaction = getFragmentManager().beginTransaction();    		    
	    transaction.replace(MainActivity.ll2_id, sdaf);
	    //transaction.addToBackStack(null);
	    transaction.commit();
	    
	    //MainActivity.mainLL.addView(ll2);
    }

	public SpannableString createTableHeaderString(String name) {		
		SpannableString spanString = new SpannableString(name);		
		spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
		spanString.setSpan(new StyleSpan(Typeface.ITALIC), 0, spanString.length(), 0);
		
		return spanString;
	}		
	
	public void disableButton() {
		addBT.setEnabled(false);
		addBT.setBackgroundDrawable(null);
		addBT.setImageDrawable(null);
		
		saveBT.setEnabled(false);		
		saveBT.setBackgroundDrawable(null);
		saveBT.setImageDrawable(null);
	}

	@Override
	public void onSaveInstanceState (Bundle outState) {		
		super.onSaveInstanceState(outState);
		
		if (sbf != null) {
			sbf.setTargetFragment(null,-1);
		}
		
		if (sdaf != null) {
			sdaf.setTargetFragment(null,-1);
		}
	}
		
	@Override
	public void onResume () {					
		super.onResume();
		
		
		if (sbf != null) {
			sbf.setTargetFragment(frg, 0);
		}
		
		if (sdaf != null) {
			sdaf.setTargetFragment(frg, 0);
		}
	}
}

