package com.bhahusut.thaipos;

import java.text.NumberFormat;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;


public class MainActivity extends ActionBarActivity {

	public static db db1;
	public static LinearLayout mainLL;	
	public MainActivity act;
	public static LinearLayout llActionCustom;
	public static final int ll1_id = R.id.ll1;
	public static final int ll2_id = R.id.ll2;
	
	Button productBT;
	Button sellBT;
	Button statBT;
	AdView adView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
				
		addCombobox2Actionbar();
		
		act = this;
		mainLL = (LinearLayout)findViewById(R.id.main_hor);
		
		if (savedInstanceState == null) {
			/*
			LinearLayout ll = new LinearLayout(this);
			ll.setOrientation(LinearLayout.HORIZONTAL);
			ll.setLayoutParams(new LinearLayout.LayoutParams(300,
					ViewGroup.LayoutParams.FILL_PARENT));
	        ll.setId(ll1_id);*/
			
			getSupportFragmentManager().beginTransaction()
					.replace(ll1_id, new productListFragment()).commit();
			//mainLL.addView(ll);
						
		}				
		
		db1 = new db(this);
		
		//ads		
		String AD_UNIT_ID =
				"ca-app-pub-4080668725279052/4693517984";
		adView = new AdView(this);
	    adView.setAdSize(AdSize.BANNER);
	    adView.setAdUnitId(AD_UNIT_ID);
	    
	    LinearLayout layout = (LinearLayout)
	    		findViewById(R.id.ads_layout);
	    layout.addView(adView);	    			    	   
	    /*
	    AdRequest adRequest = new AdRequest.Builder()
        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
        .addTestDevice("B3D229A5E2D9D063F6DF28ED85B03D1E")
        .build();*/
	    
	    AdRequest adRequest = new AdRequest.Builder().build();

	    adView.loadAd(adRequest);
	    
	}

		@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void OnDestroy() {
		db1.close();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.menu1) {
			confirmDialogFragment cdf = new confirmDialogFragment();
			cdf.ques = "ข้อมูลจะหายทั้งหมด คุณแน่ใจหรือไม่?";
			cdf.show(getSupportFragmentManager(),"แน่ใจหรือ?");			
			
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {		
		MainActivity act;
		
		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);			
			
			//db1.addSell(0, 10, 10, "datetime('now')");
			//db1.dumpTable("sell");
								
			return rootView;
		}
				
		public void onActivityCreated (Bundle savedInstanceState) {
			
			
		}
		
		public void onAttach(MainActivity activity) {
		    super.onAttach(activity);
			
			act = activity;						

		}
	}
		
	public void addCombobox2Actionbar() {//use Button
		// Set up the action bar to show tabs.
	    final ActionBar actionBar = getActionBar();
	    
	    actionBar.setDisplayShowHomeEnabled(false);
	    actionBar.setDisplayShowTitleEnabled(false);
	    actionBar.setDisplayShowCustomEnabled(true);
	    actionBar.setCustomView(R.layout.action_custom);
	    
	    productBT = 
	    		(Button)findViewById(R.id.button1);
	    productBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                								
				
				/*
				LinearLayout ll = new LinearLayout(act);
				ll.setOrientation(LinearLayout.HORIZONTAL);
				ll.setLayoutParams(new LinearLayout.LayoutParams(300,
						ViewGroup.LayoutParams.FILL_PARENT));
		        ll.setId(ll1_id);
				
		        //removeOldFragment();
		        */
				getSupportFragmentManager().beginTransaction()
						.replace(ll1_id, new productListFragment()).commit();
				/*
				mainLL.addView(ll);*/				
            }			
		});
	 
	    sellBT = 
	    		(Button)findViewById(R.id.button2);
	    sellBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                												
						        
		        getSupportFragmentManager().beginTransaction()
						.replace(ll1_id, new sellBillFragment()).commit();					
						        								
            }			
		});
	    
	    statBT = 
	    		(Button)findViewById(R.id.button3);
	    statBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                								
								
				getSupportFragmentManager().beginTransaction()
						.replace(ll1_id, new statMainFragment()).commit();															
            }			
		});
	    
	}
	
	public static void showToast(Activity context,String str) {
		Toast.makeText(context, str, 
				   Toast.LENGTH_LONG).show();
	}
	
	public static String dateEN2TH(String date) {
		if (date.isEmpty())
			return "";
		
		String[] dArr = date.split("-");
		String[] nArr = new String[3];
		nArr[0] = dArr[2];
		nArr[1] = dArr[1];
		nArr[2] = dArr[0];		
		
		return nArr[0]+"-"+nArr[1]+"-"+nArr[2];		
	}
	
	public static String dateTH2EN(String date) {
		if (date.isEmpty())
			return "";
		String[] dArr = date.split("-");
		String[] nArr = new String[3];
		nArr[0] = dArr[2];
		nArr[1] = dArr[1];
		nArr[2] = dArr[0];		
		
		return nArr[0]+"-"+nArr[1]+"-"+nArr[2];		
	}
	
	public static String formatNum(float val) {
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);
		String output = nf.format(val).toString();
		return output;
	}
	
	public static String formatNum(String sval) {
		float val = Float.parseFloat(sval);		
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);
		String output = nf.format(val).toString();
		return output;
	}
	
	public static String dtEN2TH(String dt) {
		if (dt.isEmpty())
			return "";
		String[] tmp1 = dt.split(" ");
		String tmpDate = dateEN2TH(tmp1[0]);
		return tmpDate+" "+tmp1[1];
	}
	
	public static String dtTH2EN(String dt) {
		if (dt.isEmpty())
			return "";
		String[] tmp1 = dt.split(" ");
		//Log.e("e1",tmp1[0]);
		String tmpDate = dateTH2EN(tmp1[0]);
		//Log.e("e2",tmpDate);
		return tmpDate+" "+tmp1[1];
	}
	
	@Override
	public void onActivityResult(int requestCode,int resultCode,Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
						
		if (resultCode != Activity.RESULT_OK) return;				
		if (requestCode == 3) {//confirm_dialog
			String isOk = (String)data.getSerializableExtra("isOk");	
			if (isOk.equals("yes")) {
				onConfirmOk();
			}				
		} 					
	}

	public void onConfirmOk() {
		db1.truncateDB();
		this.showToast(this,"ล้างฐานข้อมูลเรียบร้อย");
		
		//restart app 
		Intent i = getBaseContext().getPackageManager()
	             .getLaunchIntentForPackage( getBaseContext().getPackageName() );
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(i);
	}
	
	public void removeOldFragment() {
		FragmentManager fm = getSupportFragmentManager();
		int i = fm.getBackStackEntryCount();
		if (i != 0) {
			int sid = fm.getBackStackEntryAt(0).getId();
			fm.popBackStack(sid,
        		FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}
	}
	
	@Override
	public void onPause() {
		if (adView != null) {
		  adView.pause();
		}
		clearFragment();
		disableActionButton();
		
		super.onPause();
	}
	
	 @Override
	public void onDestroy() {
	    // Destroy the AdView.
	    if (adView != null) {
	      adView.destroy();
	    }
	    super.onDestroy();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
			
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		enableActionButton();
		if (adView != null) {
		  adView.resume();
		}
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
	}
	
	public void disableActionButton() {
		/*
		llActionCustom = (LinearLayout)
				findViewById(R.layout.action_custom);
		for (int i = 0; i < llActionCustom.getChildCount(); i++) {
		    View child = llActionCustom.getChildAt(i);
		    child.setEnabled(false);
		}*/
		productBT.setEnabled(false);
		sellBT.setEnabled(false);
		statBT.setEnabled(false);
	}
	
	public void enableActionButton() {
		/*llActionCustom = (LinearLayout)
				findViewById(R.layout.action_custom);
		for (int i = 0; i < llActionCustom.getChildCount(); i++) {
		    View child = llActionCustom.getChildAt(i);
		    child.setEnabled(true);
		}*/
		productBT.setEnabled(true);
		sellBT.setEnabled(true);
		statBT.setEnabled(true);
	}
	
	public void clearFragment() {
		FragmentManager fm = getSupportFragmentManager();
		fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);		
	}
	
}
