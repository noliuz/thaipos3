package com.bhahusut.thaipos;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

public class productListFragment extends Fragment {
    ListView productLV;
    ImageButton addBT;
    Button updateBT;
    EditText filterET;
    Fragment frg;
    Fragment pdf,pdaf;
    Button addBT2;
    
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.product_list, container, false); 
		
		frg = this;
    	productLV = (ListView)rootView.findViewById(R.id.listView1);    	    	        	
    	addBT = (ImageButton)rootView.findViewById(R.id.imageButton1);    	
    	filterET = (EditText)rootView.findViewById(R.id.editText1);
    	    	
        filterET.addTextChangedListener(new TextWatcher() {           
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {                
            	updateList();
                
            } 
            
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            	
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

        });
    	
    	addBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                							
				addProductDetailAddFragment();
            }			
		});
    	
    	productLV.setOnItemClickListener(new OnItemClickListener()
    	{    		
    		public void onItemClick(AdapterView<?> parent, View view,
    		    int position, long id) {
    		        addProductDetailFragment(position);    			    			
    		}
    	});
    	
    	pdf = new productDetailFragment();
    	pdaf = new productDetailAddFragment();
    	
    	updateList();    	    	    	
    	    	    	
    	return rootView;
    }
    
	@Override
	public void onAttach(Activity activity) {
	    super.onAttach(activity);
		
		//Log.e("xx",act.ll2_id+"");
		//Log.e("xx","xxxx");

	}
	
    public void updateList() {	
    	ArrayList<String> listAL = new ArrayList<String>();
    	
    	String filterStr = filterET.getText().toString();
    	//need api 16+
    	/*
    	String q = "SELECT name FROM product";
    	
    	String qf = null;
    	if (!filterStr.isEmpty())
    		 qf = " name LIKE '%"+filterStr+"%'";
    	    	
    	Cursor c = MainActivity.db1.sqliteDB.query(true, "product", 
    	new String[] {"name"},qf, null,null,null,null,null,null);   	
    	*/
    	
    	//below api by not use DISTINCT
    	String q = "SELECT name FROM product ";
    	String qf = "";    	
    	if (!filterStr.isEmpty())
    		 qf = " WHERE name LIKE '%"+filterStr+"%'";
    	q += qf;
    	q += " ORDER BY name";
    	
    	Cursor c = MainActivity.db1.query(q);    	
    	if (c.moveToFirst()) {			
			do {
				String tmp = c.getString(0);
				if (listAL.indexOf(tmp) == -1)
					listAL.add(c.getString(0));
			} while (c.moveToNext());					    	
    	}    	
    	
    	ArrayAdapter<String> ad = new ArrayAdapter<String>(getActivity(),
    			android.R.layout.simple_list_item_1,listAL);
    	productLV.setAdapter(ad);    	    	
    }
    
    public void addProductDetailFragment(int position) {
    	/*
    	LinearLayout ll2 = new LinearLayout(getActivity());
		ll2.setOrientation(LinearLayout.HORIZONTAL);
		ll2.setLayoutParams(new LinearLayout.LayoutParams(500,
				ViewGroup.LayoutParams.FILL_PARENT));
        ll2.setId(MainActivity.ll2_id);*/
													
		Bundle bd = new Bundle();
	    bd.putString("name",
	   		productLV.getItemAtPosition(position).toString());    		    	    
	    
	    pdf = new productDetailFragment();
	    pdf.setArguments(bd);    		        		    
	    
	    pdf.setTargetFragment(frg, 0);
	    	    
	    FragmentTransaction transaction = getFragmentManager().beginTransaction();    		    
	    transaction.replace(MainActivity.ll2_id, pdf);
	    //transaction.addToBackStack(null);
	    transaction.commit();
	    
	    /*MainActivity.mainLL.addView(ll2);*/
    }
    
    public void addProductDetailAddFragment() {
    	/*
    	LinearLayout ll2 = new LinearLayout(getActivity());
		ll2.setOrientation(LinearLayout.HORIZONTAL);
		ll2.setLayoutParams(new LinearLayout.LayoutParams(500,
				ViewGroup.LayoutParams.FILL_PARENT));
        ll2.setId(MainActivity.ll2_id);
			*/									    		        		 
        pdaf = new productDetailAddFragment();
        
        pdaf.setTargetFragment(frg, 0);
	    
        FragmentTransaction transaction = getFragmentManager().beginTransaction();    		    
	    transaction.replace(MainActivity.ll2_id, pdaf);
	    //transaction.addToBackStack(null);
	    transaction.commit();
	    
	    //MainActivity.mainLL.addView(ll2);
    }
    
    @Override
	public void onActivityResult(int requestCode,int resultCode,Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
						
		if (resultCode != Activity.RESULT_OK) return;				
		String isOk = (String)data.getSerializableExtra("isOk");
		Log.e("xx",isOk);
		if (isOk.equals("yes"))
			updateList();				
	}
    
    @Override
	public void onSaveInstanceState (Bundle outState) {		
		
    	super.onSaveInstanceState(outState);

		if (pdaf != null) {
			pdaf.setTargetFragment(null,-1);
		}
		if (pdf != null) {
			pdf.setTargetFragment(null,-1);
		}
				
	}
	
	@Override
	public void onResume () {		
		super.onResume();
		
		
		if (pdf != null) {
			pdf.setTargetFragment(frg, 0);
		}
		
		if (pdaf != null) {
			pdaf.setTargetFragment(frg, 0);
		}
	
		//disable button
		
	}
	
	@Override
	public void onPause() {
		super.onPause();
		clearFragment();	
	}
	
	public void clearFragment() {
		FragmentManager fm = getFragmentManager();
		fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);		
	}
}