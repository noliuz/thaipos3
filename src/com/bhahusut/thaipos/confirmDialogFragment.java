package com.bhahusut.thaipos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class confirmDialogFragment extends DialogFragment {
	Button okBT,cancelBT;
	DialogFragment frg;
	TextView quesTV;
	public static String ques="คุณแน่ใจหรือ?";	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.confirm_layout, container, false);
        
        frg = this;
        okBT = (Button)v.findViewById(R.id.button1);
        cancelBT = (Button)v.findViewById(R.id.button2);        
        quesTV = (TextView)v.findViewById(R.id.textView1);
        
        quesTV.setText(ques);
        
        cancelBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                								
				frg.dismiss();
            }			
		});
        
        okBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                								
				((MainActivity)(frg.getActivity())).onConfirmOk();
				frg.dismiss();
            }			
		});
                
        return v;
    }
	
	public void sendResult(int resultCode) {
    	if (getTargetFragment() == null)
    		return;
    	
    	
    	Intent i = new Intent();
    	i.putExtra("isOk","yes");
    	
    	getTargetFragment().onActivityResult(2, resultCode,
    			i);
    }
}
