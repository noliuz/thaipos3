package com.bhahusut.thaipos;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

public class productSelFragment extends DialogFragment {
		ListView productLV;
	    MainActivity act;
	    EditText filterET;	    
	    DialogFragment fthis;
	    ImageButton addIB;
	    
		@Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
	        // Inflate the layout for this fragment
			View rootView = inflater.inflate(R.layout.product_list, container, false); 
			fthis = this;
	    	productLV = (ListView)rootView.findViewById(R.id.listView1);    	    	        	
	    	filterET = (EditText)rootView.findViewById(R.id.editText1);
	    	addIB = (ImageButton)rootView.findViewById(R.id.imageButton1);
	    		    	
	    	addIB.setEnabled(false);
			addIB.setBackgroundDrawable(null);
			addIB.setImageDrawable(null);
	    	
	        filterET.addTextChangedListener(new TextWatcher() {           
	            @Override
	            public void onTextChanged(CharSequence s, int start, int before, int count) {                
	            	updateList();
	                
	            } 
	            
	            @Override
	            public void afterTextChanged(Editable s) {
	                // TODO Auto-generated method stub
	            	
	            }

	            @Override
	            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	                // TODO Auto-generated method stub

	            }

	        });
	    	
	    	productLV.setOnItemClickListener(new OnItemClickListener()
	    	{
	    		
	    		public void onItemClick(AdapterView<?> parent, View view,
	    		    int position, long id) {
	    		        	    			
	    		   	String pname = productLV.getItemAtPosition(position).toString();	    		   	
	    		   	sendResult(Activity.RESULT_OK,pname);
	    		   	fthis.dismiss();
	    		}
	    	});
	    	
	    	productLV.requestFocus();
	    	
	    	updateList();    	    	    	
	    	
	    	return rootView;
	    }
	    
		public void onAttach(MainActivity activity) {
		    super.onAttach(activity);
			act = activity;			
											
		}
		
	    public void updateList() {	
	    	ArrayList<String> listAL = new ArrayList<String>();
	    	
	    	String filterStr = filterET.getText().toString();
	    	//need api 16+
	    	/*String q = "SELECT name FROM product";	    	
	    	String qf = null;
	    	if (!filterStr.isEmpty())
	    		 qf = " name LIKE '%"+filterStr+"%'";
	    	    	
	    	Cursor c = act.db1.sqliteDB.query(true, "product", 
	    	new String[] {"name"},qf, null,null,null,null,null,null);   	
	    	 */
	    	
	    	//below api by not use DISTINCT
	    	String q = "SELECT name FROM product";
	    	String qf = "";    	
	    	if (!filterStr.isEmpty())
	    		 qf = " WHERE name LIKE '%"+filterStr+"%'";
	    	q += qf;
	    	
	    	Cursor c = MainActivity.db1.query(q);    	
	    	if (c.moveToFirst()) {			
				do {
					String tmp = c.getString(0);
					if (listAL.indexOf(tmp) == -1)
						listAL.add(c.getString(0));
				} while (c.moveToNext());					    	
	    	}	    	    	
	    	
	    	ArrayAdapter<String> ad = new ArrayAdapter<String>(getActivity(),
	    			android.R.layout.simple_list_item_1,listAL);
	    	productLV.setAdapter(ad);    	    	
	    }
	    
	    void sendResult(int resultCode,String pname) {
	    	if (getTargetFragment() == null)
	    		return;
	    	Intent i = new Intent();
	    	i.putExtra("pname", pname);
	    	
	    	getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode,
	    			i);
	    }
}
