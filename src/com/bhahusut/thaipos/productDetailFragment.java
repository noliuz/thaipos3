package com.bhahusut.thaipos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

public class productDetailFragment extends Fragment {
    Activity act;
    Fragment frg;
    ArrayList<DataClass.Lot> lotAL;
    ArrayList<String> lotdtAL;
    Spinner spinner1;
    EditText nameET,sell_priceET,lot_countET;
    TextView dateTV,timeTV;
    ImageButton dateBT,timeBT,submitBT,delBT;
    datepickerDialogFragment dpDF;
    timepickerDialogFragment tpTP;
    
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		View rootView = inflater.inflate(R.layout.product_detail, container, false); 
		
		frg = this;
		
		submitBT = (ImageButton)rootView.findViewById(R.id.imageButton2);
		delBT = (ImageButton)rootView.findViewById(R.id.imageButton3);
		dateBT = (ImageButton)rootView.findViewById(R.id.imageButton1);
		timeBT = (ImageButton)rootView.findViewById(R.id.imageButton4);
		spinner1 = (Spinner)rootView.findViewById(R.id.spinner1);
		nameET = (EditText)rootView.findViewById(R.id.editText1);
		sell_priceET = (EditText)rootView.findViewById(R.id.editText2);
		lot_countET = (EditText)rootView.findViewById(R.id.editText3);
		//dtTV = (TextView)rootView.findViewById(R.id.textView5);
		dateTV = (TextView)rootView.findViewById(R.id.textView7);
		timeTV = (TextView)rootView.findViewById(R.id.textView8);
		
		delBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                								
				deleteProduct();
				sendResult(Activity.RESULT_OK,"yes");
				getActivity().getSupportFragmentManager()
					.beginTransaction().remove(frg)
					.commit();
            }			
		});
		
		submitBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                								
				if (formValidation()) {
					updateProduct();
					getActivity().getSupportFragmentManager()
						.beginTransaction().remove(frg)
						.commit();
				}
            }			
		});
		
		
		dateBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {                				
				dpDF = new datepickerDialogFragment();
				dpDF.setTargetFragment(frg,1);
				dpDF.show(getFragmentManager().beginTransaction(),"เลือกวันที่");
            }			
		});
		
		timeBT.setOnClickListener(new OnClickListener () {
			public void onClick(View v) {
				tpTP = new timepickerDialogFragment();
				tpTP.setTargetFragment(frg,2);
				tpTP.show(getFragmentManager().beginTransaction(),"เลือกเวลา");											
            }			
		});
		
		spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
	        @Override
	        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
	            long arg3) {
	            // TODO Auto-generated method stub
	            updateLotDetail();
	        }
	        
	        @Override
	        public void onNothingSelected(AdapterView<?> arg0) {
	            // TODO Auto-generated method stub
	        }
	        
	    });
		
		String bundleName = getArguments().getString("name");			
		updateLot(bundleName);		
		
    	return rootView;
    }
    
	public void onAttach(Activity activity) {
	    super.onAttach(activity);
		act = activity;								
	}
	    
	public void updateDetail() {
				
	}
	
	public void updateLot(String pname) {
		String q = "SELECT * FROM product WHERE name LIKE '"+pname+"'";
		Cursor c = MainActivity.db1.query(q);
				
		lotAL = new ArrayList<DataClass.Lot>(); 
		lotdtAL = new ArrayList<String>();
		
		if (c.moveToFirst()) {			
			do {
				DataClass.Lot l = new DataClass.Lot();
				l.id = c.getString(0);
				l.name = c.getString(1);
				l.sell_price = c.getString(2);
				l.lot_count = c.getString(3);
				l.dt = c.getString(4);
				lotAL.add(l);
				
				lotdtAL.add(
					MainActivity.dtEN2TH(c.getString(4)));				
				Log.e("yy",c.getString(4));
						
			} while (c.moveToNext());					    	
    	}   
				
		ArrayAdapter<String> lotAD = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_spinner_dropdown_item,lotdtAL);
		spinner1.setAdapter(lotAD);		
	}
	
	public void updateLotDetail() {
		String lotdt = 
				spinner1.getSelectedItem().toString();
				
		int lotIndex = lotdtAL.indexOf(lotdt);
		
		nameET.setText(lotAL.get(lotIndex).name);
		sell_priceET.setText(lotAL.get(lotIndex).sell_price);
		lot_countET.setText(lotAL.get(lotIndex).lot_count);
		//dtTV.setText(lotAL.get(lotIndex).dt);
		setUIDateTime(lotAL.get(lotIndex).dt);		
	}	

	public void updateProduct() {
		String p_dt = 
				spinner1.getSelectedItem().toString();
		//Log.e("xx",p_dt);
		int idx = lotdtAL.indexOf(p_dt);		
		String p_name = lotAL.get(idx).name;
		
		String l_name = nameET.getText().toString();
		String l_sell_price = sell_priceET.getText().toString();
		String l_lot_count = lot_countET.getText().toString();
		//String l_dt = dtTV.getText().toString();
		String l_dt = getUIDateTime();
						
		String q = "UPDATE product SET ";
		q += "name='"+l_name+"'"+",";
		q += "sell_price='"+l_sell_price+"'"+",";
		q += "lot_count='"+l_lot_count+"'"+",";
		q += "dt='"+l_dt+"'";
		q += " WHERE name='"+p_name+"' AND dt='"+p_dt+"'";
		
		MainActivity.db1.query_no_res(q);
		//Log.e("db",q);				
	}	

	public void deleteProduct() {
		String p_dt = 
				spinner1.getSelectedItem().toString();
		
		int idx = lotdtAL.indexOf(p_dt);		
		String p_name = lotAL.get(idx).name;	
		String p_dt_en = MainActivity.dtTH2EN(p_dt);
				
		String q = "DELETE FROM product WHERE dt LIKE '"+p_dt_en+"' AND "
				+" name LIKE '"+p_name+"'";				
		
		MainActivity.db1.query_no_res(q);
		Log.e("db",q);				
	}	

	void sendResult(int resultCode,String isOk) {
    	if (getTargetFragment() == null)
    		return;
    	Intent i = new Intent();
    	i.putExtra("isOk", isOk);
    	
    	getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode,
    			i);
    }
	
	public boolean formValidation() {
		String l_name = nameET.getText().toString();
		String l_sell_price = sell_priceET.getText().toString();
		String l_lot_count = lot_countET.getText().toString();
		//String l_dt = dtTV.getText().toString();
		String l_dt = getUIDateTime();
		
		if (l_name.isEmpty() || l_sell_price.isEmpty()
				|| l_lot_count.isEmpty() || l_dt.isEmpty()) {
			MainActivity.showToast(getActivity(),"กรุณากรอกข้อมูลให้ครบ");
			return false;
		}
		
		return true;
	}
	
	@Override
	public void onActivityResult(int requestCode,int resultCode,Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
						
		if (resultCode != Activity.RESULT_OK) return;				
		if (requestCode == 1) {//datepicker
			String date = (String)data.getSerializableExtra("date");	
			updateDate(date);
		} else if (requestCode == 2) {//timepicker
			String time = (String)data.getSerializableExtra("time");
			updateTime(time);
		} 						
	}
	
	public void updateDate(String date) {								
		String[] dateArr = date.split("-");
		int y = Integer.parseInt(dateArr[0]);
		int m = Integer.parseInt(dateArr[1]);
		int da = Integer.parseInt(dateArr[2]);
		
		//Log.e("xx",y+"");
		
		Calendar c;
		c = Calendar.getInstance();
		c.set(y, m,da);
		DateFormat df = new DateFormat();		
		String enDate = df.format("yyyy-MM-dd",c.getTime()).toString(); 	
		
		dateTV.setText(MainActivity.dateEN2TH(enDate));		
	}
	
	public void updateTime(String time) {
		DateFormat df = new DateFormat();
		Date d = new Date();
		
		String[] timeArr = time.split(":");
		int h = Integer.parseInt(timeArr[0]);
		int m = Integer.parseInt(timeArr[1]);
		d.setHours(h);
		d.setMinutes(m);
		
		timeTV.setText(df.format("hh:mm:ss",d));		
	}
	
	
	
	public void setUIDateTime(String enDT) {
		String[] dtArr = enDT.split(" ");
		String date = dtArr[0];
		String time = dtArr[1];
		
		updateTime(time);
		updateDate(date);
	}
	
	public String getUIDateTime() {
		String date = MainActivity.dateTH2EN(dateTV.getText().toString());
		String time = timeTV.getText().toString();
		
		return date+" "+time;
	}

	@Override
	public void onSaveInstanceState (Bundle outState) {		
		super.onSaveInstanceState(outState);
		
		if (dpDF != null) {
			dpDF.setTargetFragment(null,-1);
		}
		
		if (tpTP != null) {
			tpTP.setTargetFragment(null,-1);
		}
		
		
	}
	
	@Override
	public void onResume () {		
		super.onResume();
		
		if (dpDF != null) {
			dpDF.setTargetFragment(frg,1);
		}
		
		if (tpTP != null) {
			tpTP.setTargetFragment(frg,2);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		clearFragment();	
	}
	
	public void clearFragment() {
		FragmentManager fm = getFragmentManager();
		fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);		
	}
}
